import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: () => import('@/views/Login')
  },
  {
    path: '/home',
    component: () => import('@/components/Home'),
    redirect: '/welcome',
    children: [
      { path: '/welcome', component: () => import('@/components/Welcome') },
      { path: '/users', component: () => import('@/components/user/User') },
      { path: '/rights', component: () => import('@/components/power/Rights') },
      { path: '/roles', component: () => import('@/components/power/Roles') },
      { path: '/categories', component: () => import('@/components/goods/Categories') },
      { path: '/goods', component: () => import('@/components/goods/GoodsList') },
      { path: '/goods/add', component: () => import('@/components/goods/Add.vue') },
      { path: '/orders', component: () => import('@/components/order/Order') },
      { path: '/reports', component: () => import('@/components/report /Reports') },
      { path: '/params', component: () => import('@/components/goods/Params') }]
  }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径 from 从那个路劲跳转而来 next 表示放行
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) {
    return next('/login')
  }
  next()
})
export default router
